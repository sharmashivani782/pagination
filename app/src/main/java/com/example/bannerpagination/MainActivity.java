package com.example.bannerpagination;


import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity {

    ViewPager viewPager;
    LinearLayout sliderDotspanel;
    private int dotscount;
    private ImageView[] dots;
    private Timer timer;
    private int Current_Postiion = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_main);

        viewPager = (ViewPager) findViewById (R.id.viewPager);

        sliderDotspanel = (LinearLayout) findViewById (R.id.SliderDots);

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter (MainActivity.this);

        viewPager.setAdapter (viewPagerAdapter);

        dotscount = viewPagerAdapter.getCount ();
        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView (this);
            dots[i].setImageDrawable (ContextCompat.getDrawable (getApplicationContext (), R.drawable.nonactive_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins (8, 0, 8, 0);

            sliderDotspanel.addView (dots[i], params);

        }

        dots[0].setImageDrawable (ContextCompat.getDrawable (getApplicationContext (), R.drawable.active_dot));

        viewPager.addOnPageChangeListener (new ViewPager.OnPageChangeListener () {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable (ContextCompat.getDrawable (getApplicationContext (), R.drawable.nonactive_dot));
                }

                dots[position].setImageDrawable (ContextCompat.getDrawable (getApplicationContext (), R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void Creatslideshow() {
        final Handler handler = new Handler ();
        final Runnable runnable = new Runnable () {
            @Override
            public void run() {
                if (Current_Postiion == images.length ()) {
                    Current_Postiion = 0;
                }
                viewPager.setCurrentItem (Current_Postiion++, true);
            }
        };
        timer = new Timer ();
        timer.schedule (new TimerTask () {
            @Override
            public void run() {
                handler.post (runnable);
            }
        }, 250, 2500);
    }
}